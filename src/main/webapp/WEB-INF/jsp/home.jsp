<%--

    Cloud Foundry 2012.02.03 Beta
    Copyright (c) [2009-2012] VMware, Inc. All Rights Reserved.

    This product is licensed to you under the Apache License, Version 2.0 (the "License").
    You may not use this product except in compliance with the License.

    This product includes a number of subcomponents with
    separate copyright notices and license terms. Your use of these
    subcomponents is subject to the terms and conditions of the
    subcomponent's license, as noted in the LICENSE file.

--%>
<%@ page session="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Hang w/ - Hang With Your Favorite People In Live Streaming Video </title><meta name="description" content="Hang w/ allows anyone to broadcast live mobile video to thousands of people in real time. Now you can Hang With your favorite people in live streaming video."><meta name="keywords" content="hang w, hang with, broadcast live,broadcast video,live stream,live streaming,live video,stream live,streaming video,mobile video">
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />

<!--[if lt IE 9]>
<script src="http://www.hangwith.com/assets/javascripts/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="http://www.hangwith.com/assets/stylesheets/demo.css" />
<!--[if (gt IE 8) | (IEMobile)]><!-->
<link rel="stylesheet" href="http://www.hangwith.com/assets/stylesheets/unsemantic-grid-responsive.css" />
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="http://www.hangwith.com/assets/stylesheets/ie.css" />
<![endif]-->

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />


<script src="http://www.hangwith.com/navigation/scripts/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="social/js/jquery.social.stream.wall.1.3.js"></script>
<script type="text/javascript" src="social/js/jquery.social.stream.1.5.4.js"></script>
<link rel="stylesheet" type="text/css" href="social/css/dcsns_dark.css" media="all" />
<link rel="stylesheet" type="text/css" href="social/css/dcsns_wall.css" media="all" />

<script type="text/javascript" src="//use.typekit.net/jar6vgt.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="http://jwpsrv.com/library/WS7J7tRSEeKwfxIxOQulpA.js"></script>

<style type="text/css">
    #newmember
    {
        width:120px;
        height:145px;
        float:left;
        margin-bottom:50px;
        margin-left:25px;
        overflow:hidden;
    }
    #newmember img
    {
        border:3px solid #636466;
    }

    #navlist
    {
        float:right !important;
    }

</style>


<style type="text/css">

/* all */
::-webkit-input-placeholder { color:#5b5c5e; font-size:14px; padding-bottom:5px; }
::-moz-placeholder { color:#5b5c5e; font-size:14px; padding-bottom:5px;} /* firefox 19+ */
:-ms-input-placeholder { color:#5b5c5e; font-size:14px; padding-bottom:5px;} /* ie */
input:-moz-placeholder { color:#5b5c5e; font-size:14px;padding-bottom:5px;}


.dcsns-toolbar
{
    margin-left:30%;
    margin-bottom:50px;
}

input[type="text"]
{
    font-size:14px;
}

textarea {
    font-size: 12pt;
    color:#5b5c5e;
}

#bigtextcontainer
{
    margin-top:150px;
}

.styled-select {
    width: 500px;
    height: 40px;
    margin-top:20px;
    overflow: hidden;
    background: url('http://www.hangwith.com/images/select_arrow.png') no-repeat right #f5f5f5;
    border: 1px solid #ccc;
}

.styled-select select {
    background: transparent;
    color:#5b5c5e;
    width: 500px;
    padding: 5px;
    font-size: 14px;
    line-height: 1;
    border: 0;
    border-radius: 0;
    height: 34px;
    -webkit-appearance: none;
    float:left;
}


.feature_icon
{
    margin-top:40px;
}

#navlist li
{
    display: inline;
    list-style-type: none;
    padding-right: 0px;
}

#navlist a
{
    color:#FFFFFF;
    font-size:18px;
}

#androidbutton, #iosbutton
{
    width:20%;
    margin-top:40px;
}

body
{
    color:#545456;
}


.feature:hover
{
    color:#20aae2;
    cursor:pointer;
}

.feature
{
    width:400px;
}


/* TABLET ONLY */
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
    #headerbackground
    {
        height:50% !important;
    }

    #phonegraphic
    {
        width:100%;
        margin-top:100px;
    }

    .feature_icon
    {
        margin-top:10px;
    }

    .feature
    {
        width:300px;
    }


    .dcsns-toolbar .filter .link-all
    {
        height:60px !important;
    }



}


/* TABLET PORTRAIT */

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) {
    #bigtext
    {
        width:90% !important;
    }

    #navlist
    {
        display:none;
    }

    #headerbackground
    {
        height:10% !important;
    }

    .dcsns-toolbar
    {
        margin-left:20%;
        margin-bottom:50px;
    }


}




/* TABLET LANDSCAPE */

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : landscape) {
    #headerbackground
    {
        height:20% !important;
    }

    #desktopnav
    {
        display:none !important;
    }

    #bigtext
    {
        width:80% !important;
    }

    #navlist
    {
        display:none;
    }

    .dcsns-toolbar
    {
        margin-left:30%;
        margin-bottom:50px;
    }


}






/* ALL MOBILE DEVICES */
@media only screen and (max-width: 768px) {
    #logowhite
    {
        width:50px;
    }



    #social-text
    {
        width:100% !important;
    }



    #androidbutton, #iosbutton
    {
        width:40% !important;
    }


    #desktopnav
    {
        display:none !important;
    }

}

/* #### Mobile Phones Portrait #### */
@media screen and (max-device-width: 480px) and (orientation: portrait){
    #bigtext
    {
        width:80% !important;
    }

    .statdescription
    {
        font-size:12px !important;
    }

    .dcsns-toolbar
    {
        display:none;
    }

    #bigtextcontainer
    {
        margin-top:100px; !important;
    }

    #headerbackground
    {
        height:10% !important;
    }

    .feature
    {
        width:200px;
    }
    .hw_large_footer
    {
        max-width:100%;
    }
    .dlios
    {
        width:75%;
    }
    .dldroid
    {
        width:100%;
    }
    .mobile-break
    {
        clear:both;
        height:15px;
    }

    #phonegraphic
    {
        width:100%;
        margin-left:20%;
        margin-bottom:20px;
    }


    input[type="text"]
    {
        width:275px !important;
    }

    .styled-select
    {
        width:275px !important;
    }

    .styled-select select
    {
        width:275px !important;
    }

    textarea {
        width:275px !important;
    }



}


/* #### Mobile Phones Landscape #### */
@media screen and (max-device-width: 640px) and (orientation: landscape){

    #headerbackground
    {
        height:20% !important;
    }

    .dcsns-toolbar
    {
        display:none;
    }

    .hw_large_footer
    {
        max-width:100%;
    }

    #phonegraphic
    {
        width:100%;
        margin-left:20%;
        margin-bottom:20px;
    }
}



</style>



</head>
<body>



<!--slide 1-->
<div id="headerbackground" style="background-color:#00a9e4; width:100%; height:20%; position:relative; text-align:center;">

    <div class="grid-container">
        <a style="height:75px;" href="http://www.hangwith.com"><img id="logowhite" src="http://www.hangwith.com/images/logo-white.png" style="position:relative; top:20px; right:40%; height:50px"></a>

        <div id="desktopnav" style="left:40px; position:relative; top:-20px;">
            <ul style="float:right;" id="navlist">
                <li><a href="http://www.hangwith.com/about" id="current">About</a></li>
                <li><a href="http://www.hangwith.com/new">Who's Who</a></li>
                <li><a href="press">Press</a></li>
                <li><a href="http://www.hangwith.com/advertising">Advertising & Sponsorships</a></li>
                <li><a href="http://www.hangwith.com/social-media">The Latest</a></li>
                <li><a href="http://www.hangwith.com/events/sxsw">Events</a></li>
                <li><a href="http://www.hangwith.com/blog">Blog</a></li>
                <li><a href="http://www.hangwith.com/faq">FAQ</a></li>
                <li><a href="http://www.hangwith.com/contact">Contact</a></li>
            </ul>

        </div>
    </div>
</div>

<!--slide 1 -->
<div class="container">

    <sec:authentication var="user" property="principal" />
    <div>
        <h2>Hi ${user.name}</h2>

        <p>Your account login is working and you have authenticated.</p>

        <c:if test="${error!=null}">
            <div class="error" title="${error}">
                <p>But there was an error.</p>
            </div>
        </c:if>

        <h2>You are logged in  : ${user.email}</h2>

        <p>
            <c:url value="/logout.do" var="url" />
            <a href="${fn:escapeXml(url)}">Logout</a> &nbsp;
        </p>
        <a href="/app/search">Search Users</a>
    </div>


</div>




</div>

<!-- slide 2 -->

<div style="clear:both; height:20px;"> </div>







<!-- mobile footer -->


</div>
</div>


<!-- slide 7 -->






<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-8158474-27', 'hangwith.com');
    ga('send', 'pageview');

</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-58BQ6X"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-58BQ6X');</script>
<!-- End Google Tag Manager -->






</div>
<script src="http://www.hangwith.com/assets/javascripts/demo.js"></script>


</body>
</html>

