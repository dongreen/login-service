package com.hangwith.login.config;

import org.cloudfoundry.identity.uaa.client.SocialClientUserDetailsSource;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.web.client.RestTemplate;

//@Configurable
public class SocailLoginConfig {

	@Bean
	RestTemplate restTemplate(){
		return new OAuth2RestTemplate(new AuthorizationCodeResourceDetails(), new DefaultOAuth2ClientContext());
	}
	
	@Bean
	SocialClientUserDetailsSource fbClientUserDetailsSource(){		
		SocialClientUserDetailsSource filter = new SocialClientUserDetailsSource();
		filter.setRestTemplate(restTemplate());
		filter.setUserInfoUrl("https://graph.facebook.com/me");
		return filter;
	}
	  
	
}
